package com.shiva.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DemoController {

	@RequestMapping(path= "/greet/{name}",method=RequestMethod.GET)
	//@ResponseBody
	public String greet(@PathVariable String name, ModelMap model){
		String greet =" Hello !!!" + name + " How are You?";
		model.addAttribute("greet", greet);
		System.out.println(greet);
		return "greet";
	    }

}
