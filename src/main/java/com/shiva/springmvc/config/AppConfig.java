package com.shiva.springmvc.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.shiva.springmvc"})
public class AppConfig implements WebApplicationInitializer {

 public void onStartup(ServletContext ctx) throws ServletException {
	 AnnotationConfigWebApplicationContext webCtx = new AnnotationConfigWebApplicationContext();
	 webCtx.register(AppConfig.class);
	 webCtx.setServletContext(ctx);
	 ServletRegistration.Dynamic servlet = ctx.addServlet("dispatcher", new DispatcherServlet(webCtx));
	 servlet.setLoadOnStartup(1);
	 servlet.addMapping("/");
	 }
 
 @Bean
 public ViewResolver viewResolver() {

InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
 viewResolver.setViewClass(JstlView.class);
 viewResolver.setPrefix("/WEB-INF/pages/");
  viewResolver.setSuffix(".jsp");

return viewResolver;
}

}
